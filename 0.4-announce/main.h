#pragma once
#define ANNOUNCE_AGENT "VCMP/0.4"
#define ANNOUNCE_HOST "master.vc-mp.org"
#define ANNOUNCE_PATH "/announce.php"
#define ANNOUNCE_PORT 80

#include <stdio.h>
#include <string.h>
#if defined( WIN32 ) || defined( WIN64 )
	#include <winsock2.h>
	#include <ws2tcpip.h>
	#include <windows.h>

	#define EXPORT __declspec(dllexport)
	#define SOCKET_TYPE SOCKET
	#define IOCTL ioctlsocket
	#define WOULD_BLOCK WSAEWOULDBLOCK
#else
	#include <sys/socket.h>
	#include <sys/ioctl.h>
	#include <sys/types.h>
	#include <sys/time.h>
	#include <netinet/in.h>
	#include <arpa/inet.h>
	#include <netdb.h>
	#include <errno.h>
	#include <unistd.h>

	// These are apparently Windows only defines. So on UNIX lets define them ourselves
	#ifndef INVALID_SOCKET
		#define INVALID_SOCKET -1
	#endif

	#ifndef SOCKET_ERROR
		#define SOCKET_ERROR -1
	#endif

	#define EXPORT
	#define SOCKET_TYPE int
	#define closesocket close
	#define IOCTL ioctl
	#define WOULD_BLOCK EWOULDBLOCK
	#define SD_SEND SHUT_WR
#endif