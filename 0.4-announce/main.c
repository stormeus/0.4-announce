#include "main.h"
#include "plugin.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#define MAX_ANNSERVERS 5
#ifdef WIN32
	#define WIN32_LEAN_AND_MEAN
	#define WORKER DWORD WINAPI
	#define THREAD_TYPE HANDLE
	#define snprintf _snprintf
	#define TIMEOUT DWORD

	#include <Windows.h>
#else
	#include <pthread.h>
	#include <unistd.h>

	#define SOCKET int
	#define WORKER void*
	#define THREAD_TYPE pthread_t
	#define BOOL int
	#define TRUE 1
	#define FALSE 0
	#define stricmp strcasecmp
	#define TIMEOUT struct timeval
#endif

int last_errno;
PluginFuncs* sdk;
BOOL be_verbose = FALSE;
volatile BOOL is_running = TRUE;
BOOL has_thread = FALSE;
THREAD_TYPE thread;
time_t last_announce = 0;
char announce_host[MAX_ANNSERVERS][64] = { { 0 }, { 0 }, { 0 }, { 0 }, { 0 } };

typedef enum {
	ANNERR_OK,
	ANNERR_SOCKNOTOPENED,
	ANNERR_LOOKUPFAIL,
	ANNERR_NOHOST,
	ANNERR_CONNERR,
	ANNERR_WRITEFAIL,
	ANNERR_READFAIL,
	ANNERR_STOREFAIL,
	ANNERR_PARSEFAIL,
	ANNERR_SOCKOPTERR
} ErrorType;

void sleepfor(uint32_t time) {
	#ifdef WIN32
		Sleep(time * 1000);
	#else
		struct timespec tim, tim2;
		tim.tv_sec = time;
		tim.tv_nsec = 0;

		if (nanosleep(&tim, &tim2) < 0 && be_verbose) {
			sdk->LogMessage("announcer: failed to sleep for %d seconds. Interrupted?", time);
		}
	#endif
}

int geterror(void) {
#ifdef WIN32
	return WSAGetLastError();
#else
	return errno;
#endif
}

void SplitStringWithNull(char* sz) {
	if (sz) {
		*sz = '\0';
	}
}

unsigned int AnnounceToServer(const char* host, const char* data, long int* out_http_code) {
	unsigned int ann_port = ANNOUNCE_PORT,
				 server_port = 0,
				 server_version = 0;

	char endpoint[64] = { 0 },
		 agent[32] = { 0 },
		 message[512] = { 0 },
		 response[512] = { 0 },

		 *port_separator = strchr(host, ':'),
	     *endpoint_separator = strchr(host, '/'),
		 *agent_separator = strchr(host, '|');

	BOOL recv_timeout_set, send_timeout_set;
	SOCKET sockfd;
	TIMEOUT timeout;
	struct addrinfo hints, *result, *it;
	int bytes, sent, received, total, matches, http_code, e, return_code = ANNERR_OK;

	ServerSettings set;
	sdk->GetServerSettings(&set);

	server_version = sdk->GetServerVersion();
	server_port = set.port;

	if (server_port == 0 || server_version == 0) {
		sdk->LogMessage("announcer: Failed to get server port or version. Something's wrong.");
	}

	if (port_separator && port_separator[1] != '\0') {
		// Pointer magic: we can change "host" to terminate where
		// the port separator is (or used to be) by dereferencing
		// the port separator string, as it points to the original
		// host string anyway.
		//
		// The takeaway of this is don't be afraid of what's going
		// on in this code. This is valid.
		port_separator[0] = '\0';
		ann_port = atoi(&port_separator[1]);
	}

	snprintf(endpoint, 64, "%s", (endpoint_separator ? endpoint_separator : ANNOUNCE_PATH));
	SplitStringWithNull(endpoint_separator);

	snprintf(agent, 32, "%s", (agent_separator && agent_separator[1] != '\0' ? &agent_separator[1] : ANNOUNCE_AGENT));
	SplitStringWithNull(agent_separator);

	snprintf(message, 512,
		"POST %s HTTP/1.1\r\n"
		"Host: %s\r\n"
		"Connection: close\r\n"
		"User-Agent: %s\r\n"
		"VCMP-Version: %u\r\n"
		"Content-Type: application/x-www-form-urlencoded\r\n"
		"Content-Length: %u\r\n"
		"\r\n"
		"%s",
		
		endpoint,
		host,
		agent,
		server_version,
		strlen(data),
		data);

	// http://stackoverflow.com/a/22135885
	// http://linux.die.net/man/3/getaddrinfo
	memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;

	e = getaddrinfo(host, NULL, &hints, &result);
	if (e != 0) {
		if (out_http_code) {
			*out_http_code = result;
		}

		last_errno = geterror();
		return ANNERR_LOOKUPFAIL;
	}

	for (it = result; it != NULL; it = it->ai_next) {
		if (be_verbose) {
			sdk->LogMessage("announcer: Candidate for hostname found, trying to connect...");
		}

		sockfd = socket(it->ai_family, it->ai_socktype, it->ai_protocol);
		if (sockfd == -1 || sockfd == (~0)) {
			if (be_verbose) {
				sdk->LogMessage("announcer: Encountered errno %d opening socket in hostlookup", geterror());
			}

			continue;
		}

#ifdef WIN32
		timeout = 15000;

		send_timeout_set = (setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, (char*)&timeout, sizeof(timeout)) == 0);
		recv_timeout_set = (setsockopt(sockfd, SOL_SOCKET, SO_SNDTIMEO, (char*)&timeout, sizeof(timeout)) == 0);
#else
		timeout.tv_sec = 15;
		timeout.tv_usec = 0;

		send_timeout_set = (setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, (char*)&timeout, sizeof(timeout)) == 0);
		recv_timeout_set = (setsockopt(sockfd, SOL_SOCKET, SO_SNDTIMEO, (char*)&timeout, sizeof(timeout)) == 0);
#endif

		if (!send_timeout_set || !recv_timeout_set) {
			last_errno = geterror();
			return_code = ANNERR_SOCKOPTERR;

			break;
		}

		if (it->ai_family == AF_INET) {
			((struct sockaddr_in*)(it->ai_addr))->sin_port = htons(ann_port);
		}
		else {
			((struct sockaddr_in6*)(it->ai_addr))->sin6_port = htons(ann_port);
		}
		
		if (connect(sockfd, it->ai_addr, it->ai_addrlen) != 0) {
			return_code = ANNERR_CONNERR;
		}

		break;
	}

	if (it == NULL) {
		return_code = ANNERR_NOHOST;
	}

	if (return_code != ANNERR_OK) {
		goto fail;
	}

	total = strlen(message);
	sent = 0;

	do {
		bytes = send(sockfd, message + sent, total - sent, 0);
		if (bytes < 0) {
			return_code = ANNERR_WRITEFAIL;
			last_errno = geterror();

			break;
		}

		if (bytes == 0) {
			break;
		}

		sent += bytes;
	} while (sent < total);

	if (be_verbose) {
		sdk->LogMessage("announcer: Sent %d/%d bytes to %s", sent, total, host);
	}

	if (return_code != ANNERR_OK) {
		goto fail;
	}

	memset(response, 0, sizeof(response));
	total = sizeof(response) - 1;
	received = 0;

	do {
		bytes = recv(sockfd, response + received, total - received, 0);
		if (be_verbose) {
			sdk->LogMessage("--> %d bytes in", bytes);
		}

		if (bytes < 0) {
			return_code = ANNERR_READFAIL;
			last_errno = geterror();

			break;
		}

		if (bytes == 0) {
			break;
		}

		received += bytes;
	} while (received < total);

	if (be_verbose) {
		sdk->LogMessage("announcer: Received %d/%d bytes from %s", received, total, host);
		sdk->LogMessage("announcer: %s", response);
	}

	if (received == total) {
		return_code = ANNERR_STOREFAIL;
	}

	if (return_code != ANNERR_OK) {
		goto fail;
	}

	matches = sscanf(response, "HTTP/%*3c %d %*s", &http_code);
	if (matches < 1) {
		return_code = ANNERR_PARSEFAIL;
		goto fail;
	}

	if (out_http_code) {
		*out_http_code = http_code;
	}

fail:
    freeaddrinfo(result);
	closesocket(sockfd);
	if (return_code != ANNERR_OK) {
		*out_http_code = -1;
	}

	return return_code;
}

WORKER WorkOnAnnouncing(void* data) {
	char post_data[16] = { 0 };
	long http_code, ret_code;
	int i;

	while (is_running) {
        if (time(NULL) - last_announce < 60) {
            sleepfor(1);
            continue;
        }
	
    	ServerSettings settings;
	    sdk->GetServerSettings(&settings);
    	snprintf(post_data, 16, "port=%i", settings.port);

		for (i = 0; i < MAX_ANNSERVERS; i++) {
			if (announce_host[i][0] == '\0') {
				continue;
			}

			ret_code = AnnounceToServer(announce_host[i], post_data, &http_code);
			if (ret_code != ANNERR_OK || http_code != 200) {
				switch (ret_code) {
					case ANNERR_CONNERR:
						sdk->LogMessage("announcer: Failed to announce to %s; socket failed to connect to host (%d)", announce_host[i], last_errno);
						break;

					case ANNERR_LOOKUPFAIL:
						sdk->LogMessage("announcer: Failed to announce to %s; failed to lookup host address (%d, %s)", announce_host[i], http_code, gai_strerror(http_code));
						break;

					case ANNERR_NOHOST:
						sdk->LogMessage("announcer: Failed to announce to %s; no suitable host address found", announce_host[i]);
						break;

					case ANNERR_PARSEFAIL:
						sdk->LogMessage("announcer: A request was made to %s but the response couldn't be understood", announce_host[i]);
						break;

					case ANNERR_READFAIL:
						sdk->LogMessage("announcer: Failed to read announce response from %s (%d)", announce_host[i], last_errno);
						break;

					case ANNERR_SOCKNOTOPENED:
						sdk->LogMessage("announcer: Failed to announce to %s; socket failed to open", announce_host[i]);
						break;

					case ANNERR_STOREFAIL:
						sdk->LogMessage("announcer: Failed to store response from %s", announce_host[i]);
						break;

					case ANNERR_WRITEFAIL:
						sdk->LogMessage("announcer: Failed to announce to %s; failed to write request to socket (%d)", announce_host[i], last_errno);
						break;

					case ANNERR_SOCKOPTERR:
						sdk->LogMessage("announcer: Failed to set timeout options on socket (%d)", last_errno);
						break;

					case ANNERR_OK:
						switch (http_code) {
							case 400:
								sdk->LogMessage("announcer: %s denied request due to malformed data", announce_host[i]);
								break;

							case 403:
								sdk->LogMessage("announcer: %s denied request, server version may not have been accepted", announce_host[i]);
								break;

							case 405:
								sdk->LogMessage("announcer: %s denied request, GET is not supported", announce_host[i]);
								break;

							case 408:
								sdk->LogMessage("announcer: %s timed out while trying to reach your server; are your ports forwarded?", announce_host[i]);
								break;

							case 500:
								sdk->LogMessage("announcer: %s had an unexpected error while processing your request", announce_host[i]);
								break;

							case 200:
							default:
								break;
						}

						break;
				}
			}
			else if (be_verbose) {
				sdk->LogMessage("announcer: Successfully announced to %s", announce_host[i]);
			}
		}
	}

#ifndef WIN32
	return 0;
#endif
}

uint8_t OnInitServer(void) {
		#ifdef WIN32
		thread = CreateThread(NULL, 0, WorkOnAnnouncing, NULL, 0, NULL);
		if (thread == NULL) {
			DWORD dw = GetLastError();
			sdk->LogMessage("announcer: Can't create a worker thread for announcing.");
			sdk->LogMessage("announcer: Windows error %d encountered while trying to do so.", dw);
		}
		else if (be_verbose) {
			sdk->LogMessage("announcer: Thread created, ready to announce.");
            has_thread = TRUE;
		}
	#else
		int err = pthread_create(&thread, NULL, &WorkOnAnnouncing, NULL);
		if (err != 0) {
			sdk->LogMessage("announcer: Can't create a worker thread for announcing.");
			sdk->LogMessage("announcer: Errcode %d: %s", err, strerror(err));
		}
		else if (be_verbose) {
			sdk->LogMessage("announcer: Ready to announce to masterlist(s).");
            has_thread = TRUE;
		}
	#endif

	return 1;
}

void OnShutdownServer(void) {
    is_running = FALSE;
    if (has_thread) {
    #ifdef WIN32
        WaitForSingleObject(thread, INFINITE);
    #else
        pthread_join(thread, NULL);
    #endif
    }
}

EXPORT unsigned int VcmpPluginInit(PluginFuncs* functions, PluginCallbacks* callbacks, PluginInfo* info)
{
	FILE* file;
	info->pluginVersion = 0x1000;
    info->apiMajorVersion = PLUGIN_API_MAJOR;
    info->apiMinorVersion = PLUGIN_API_MINOR;
	strcpy(info->name, "0.4-announce");

	file = fopen("server.cfg", "r");
	if (file != NULL) {
		char szLine[512];
		char szProperty[64];
		int multicast = 0;

		while (fgets(szLine, sizeof(szLine) / sizeof(szLine[0]), file)) {
			sscanf(szLine, "%63s", szProperty);
			szProperty[63] = '\0';

			if (stricmp(szProperty, "announce") == 0) {
				if (sscanf(szLine, "%63s %63[^\n]s", szProperty, announce_host) != 2)
					functions->LogMessage("Warning: parameters for 'announce' in config were not as expected\n");

				announce_host[multicast++][63] = '\0';
			}
			else if (stricmp(szProperty, "announce_verbose") == 0) {
				be_verbose = TRUE;
				functions->LogMessage("announcer: Verbosity enabled");
			}
		}

		fclose(file);
	}

	if (announce_host[0][0] == 0)
		strcpy(announce_host[0], ANNOUNCE_HOST);

	sdk = functions;
	callbacks->OnServerInitialise = OnInitServer;
	callbacks->OnServerShutdown = OnShutdownServer;

	return 1;
}
